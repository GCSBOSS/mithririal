
function StyleGuide(){

    return {
        view: function () {
            return m('main', [
                m('h1', 'Mithririal Style Guide'),

                m(m.Button),
                m(m.Button, { type: 'text' }),
                m(m.Button, { type: 'contained' }),

                m(m.Button, { icon: 'star' }),
                m(m.Button, { icon: 'done', type: 'text' }),
                m(m.Button, { icon: 'warning', type: 'contained' }),

                m(m.IconButton, { icon: 'menu' }),

                m(m.TextField, { label: 'Name' }),
                m(m.TextField, { label: 'Full Name', outlined: true }),

                m(m.TextField, { label: 'Name', helper: 'My little helper' }),
                m(m.TextField, { outlined: true, error: 'My nasty error', req: true }),

                m(m.RedactedField, { label: 'Password', outlined: true }),

                m(m.TextField, { label: 'Full Name', icon: 'star', text: 'John Doe' }),
                m(m.TextField, { label: 'Name', outlined: true, icon: 'face' }),

                m(m.TextField, { label: 'Full Name', clear: true, text: 'John Doe' }),

                m(m.TextField, { label: 'Full Name', prefix: '$' }),
                m(m.TextField, { label: 'Full', outlined: true, suffix: 'lbs' }),

                m(m.TextField, { label: 'Full', placeholder: 'Your Name here' }),
                m(m.TextField, { label: 'Full Name', disabled: true, text: 'John Doe' }),

                m(m.TextField, { label: 'Name', outlined: true, disabled: true }),

                m(m.Material, { z: 4 }, 'Text content Inside'),

                m(m.AppBar, { title: 'My Cool App', actions: { search: 0, 'more_vert': 0 } }),
                m(m.AppBar, { back: 'test', prominent: true, title: 'My Prominent App', actions: { search: 0, 'more_vert': 0 } }),
                m(m.AppBar, { title: 'My Cool App', nav: {},  actions: { search: 0, 'more_vert': 0 } })


            ])
        }
    }
}

StyleGuide;
