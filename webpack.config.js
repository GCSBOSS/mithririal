const path = require('path');

module.exports = {
    output: {
        path: path.resolve(__dirname, 'dist'),
        filename: 'mithririal.min.js',
        library: 'mithririal'
    },
    externals: [
        'mithril'
    ]
};
