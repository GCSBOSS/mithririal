
export function parseClasses(...specs){
    let r = '';
    for(let s of specs)
        if(Array.isArray(s))
            r += parseClasses(...s);
        else if(typeof s == 'string')
            r += '.' + s;
    return r;
}
