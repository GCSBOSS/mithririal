
import m from 'mithril';
import { TextField, RedactedField } from './text-field';
import { Button, IconButton } from './button';
import { Material } from './material';
import { AppBar } from './app-bar';

const Mithririal = {
    TextField,
    RedactedField,
    Button,
    IconButton,
    Material,
    AppBar
};

Object.assign(m, Mithririal);

export default m;
