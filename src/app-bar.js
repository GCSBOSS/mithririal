
import { parseClasses } from './util';

export function AppBar(){

    return {

        view(vnode){

            let attrs = {
                tagname: 'header',
                classes: [ 'appbar', vnode.attrs.prominent ? 'prominent' : '' ],
                z: 4
            };

            let actions = [];

            let nav = null;

            //if(typeof vnode.attrs.nav == 'object')
            //    nav =


            if(typeof vnode.attrs.back == 'string')
                nav = m(IconButton, {
                    icon: 'arrow_back',
                    action: m.route.set(vnode.attrs.back/* TODO , input*/)
                });

            if(typeof vnode.attrs.actions == 'object')
                for(let icon in vnode.attrs.actions)
                    actions.push(m(IconButton, { icon, action: vnode.attrs.actions[icon] }));

            return m(Material, attrs,
                nav,
                vnode.attrs.title && m('h1', vnode.attrs.title),
                ...actions
            );
        }

    };

}
