
import { parseClasses } from './util';

export function Material(){

    return {

        view(vnode){
            let tag = vnode.attrs.tagname || 'div';

            delete vnode.attrs.tagname;

            if(vnode.attrs.z){
                vnode.attrs['data-z'] = vnode.attrs.z;
                delete vnode.attrs.z;
            }

            let cls = parseClasses(vnode.attrs.classes);
            delete vnode.attrs.classes;

            return m(
                tag + '.material' + cls,
                vnode.attrs,
                vnode.children
            );
        }

    };

}

// function Card(){
//
//     return {
//
//         view(vnode){
//
//             if(typeof vnode.attrs.action == 'function')
//                 var onclick = vnode.attrs.action;
//
//             return m(
//                 'article.card' + parseClasses(vnode.attrs.class),
//                 { onclick },
//                 vnode.children
//             );
//         }
//
//     };
// }
