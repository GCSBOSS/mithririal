
import { parseClasses } from './util';

export function TextField(){

    return {

        view(vnode){

            let placeholder = vnode.attrs.placeholder || ' ';

            if(vnode.attrs.error instanceof Error)
                vnode.attrs.error = vnode.attrs.error.message;


            let attrs = {};

            // TODO maybe function to move stuff to dataset.
            vnode.attrs.prefix && (attrs['data-prefix'] = vnode.attrs.prefix);
            vnode.attrs.suffix && (attrs['data-suffix'] = vnode.attrs.suffix);

            let dc = vnode.attrs.disabled ? 'disabled' : '';
            let oc = vnode.attrs.outlined ? 'outline' : '';
            let ec = vnode.attrs.error ? 'error' : '';

            let req = vnode.attrs.req ? '*' : '';

            let type = vnode.attrs._type || 'text';
            let value = vnode.attrs.text || '';

            return m('label.text-field' + parseClasses(vnode.attrs.classes, dc, oc, ec),
                attrs,

                // Leading Icon
                vnode.attrs.icon && m('span.icon', vnode.attrs.icon),

                // Helper & Error
                (vnode.attrs.helper || vnode.attrs.error)
                    && m('span.helper', vnode.attrs.error || vnode.attrs.helper),

                // Input
                m('input', { value, type, placeholder, disabled: Boolean(vnode.attrs.disabled) }),

                typeof vnode.attrs._children == 'function' && vnode.attrs._children(),

                // Label
                m('span', (vnode.attrs.label || 'Text Field') + req)
            );
        }

    };
}

export function RedactedField(){

    let show = false;

    return {

        view(vnode){

            let opts = { ...vnode.attrs };

            delete opts.prefix;
            delete opts.suffix;
            opts._type = show ? 'text' : 'password';
            opts.classes = [ 'redacted', opts.classes ];

            opts._children = () => m(IconButton, {
                icon: show ? 'visibility_off' :'visibility',
                action: e => { show = !show; e.preventDefault(); }
            });

            return m(TextField, opts);
        }

    };
}
