
import { parseClasses } from './util';

export function Button(){

    return {

        view(vnode){

            let type = '';
            if(vnode.attrs.type in { text: 1, contained: 1 })
                type = '.' + vnode.attrs.type;

            if(typeof vnode.attrs.action == 'function')
                var onclick = vnode.attrs.action;

            if(typeof vnode.attrs.icon == 'string')
                var icon = vnode.attrs.icon;

            return m(
                'button[type=button]' + type + parseClasses(vnode.attrs.classes),
                { onclick, 'data-icon-pre': icon },
                vnode.attrs.text || 'Button',
                m('span.effects')
            );
        }

    };
}


export function IconButton(){

    return {

        view(vnode){

            if(typeof vnode.attrs.action == 'function')
                var onclick = vnode.attrs.action;

            if(typeof vnode.attrs.icon == 'string')
                var icon = vnode.attrs.icon;

            return m(
                'button[type=button]',
                { onclick, 'data-icon': icon },
                m('span.effects')
            );
        }

    };
}
